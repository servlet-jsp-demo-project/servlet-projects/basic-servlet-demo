package com.cdac;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet(value = "/test",loadOnStartup = 1)
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		System.out.println("Init : " + this.getClass().getName());
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("doGet : " + req.getClass().getName());
		resp.setContentType("text/html");
		try (PrintWriter pw = resp.getWriter()) {
			pw.print("Hello From Servlet : Class Name : " + req.getClass().getName()); // Output:
																						// org.apache.catalina.connector.RequestFacade
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void destroy() {
		System.out.println("destroy : " + this.getClass().getName());
	}

}
